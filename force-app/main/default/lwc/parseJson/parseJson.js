import { LightningElement } from 'lwc';
		const columns = [
    { label: 'in', fieldName: 'in' , type: 'text'},
    { label: 'loc', fieldName: 'loc', type: 'text' }
];
export default class ParseJson extends LightningElement {

    data = [];
    columns = columns;

    connectedCallback() {
				this.data  = JSON.stringify('{0 : {"in":"mmm","loc":"1234"},1: {"in" :"mmm", "loc" :"1234"}}');   
    }
}